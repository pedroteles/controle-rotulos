package zeus.control;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import zeus.Global;
import zeus.dao.EntradaDAO;
import zeus.dao.RotuloDAO;
import zeus.dao.SaidaDAO;
import zeus.model.Entrada;
import zeus.model.Rotulo;
import zeus.model.Saida;

import java.io.IOException;

public class BancoDadosController {
    public TableView<Rotulo> tbvRotulos;
    public TableColumn<Rotulo, Integer> tcIdRotulo;
    public TableColumn<Rotulo, String> tcNomeRotulo;
    public TableView<Entrada> tbvEntradas;
    public TableColumn<Entrada, Integer> tcIdEntrada;
    public TableColumn<Entrada, String> tcDataEntrada;
    public TableView<Saida> tbvSaidas;
    public TableColumn<Saida,Integer> tcIdSaida;
    public TableColumn<Saida, String> tcDataSaida;

    public void initialize() {
        RotuloDAO rotuloDAO = new RotuloDAO();
        EntradaDAO entradaDAO = new EntradaDAO();
        SaidaDAO saidaDAO = new SaidaDAO();

        tcIdRotulo.setCellValueFactory(new PropertyValueFactory<>("Id"));
        tcNomeRotulo.setCellValueFactory(new PropertyValueFactory<>("NomeRotulo"));

        tcIdEntrada.setCellValueFactory(new PropertyValueFactory<>("Id"));
        tcDataEntrada.setCellValueFactory(new PropertyValueFactory<>("Data"));

        tcIdSaida.setCellValueFactory(new PropertyValueFactory<>("Id"));
        tcDataSaida.setCellValueFactory(new PropertyValueFactory<>("Data"));

        for (Rotulo item: rotuloDAO.selecionarTodosRotulos()) {
            tbvRotulos.getItems().add(item);
        }

        for (Entrada item: entradaDAO.selecionarTodasEntradas()) {
            String novaData = "";
            String ano = "";
            String mes = "";
            String dia = "";

            int hifens = 0;

            for(int i = 0; i < item.getData().length(); i++) {
                if(i == 4 || i == 7) {
                    hifens++;
                    i++;
                }

                switch(hifens) {
                    case 0:
                        ano += item.getData().charAt(i);
                        break;
                    case 1:
                        mes += item.getData().charAt(i);
                        break;
                    case 2:
                        dia += item.getData().charAt(i);
                        break;
                }
            }

            novaData = dia + "/" + mes + "/" + ano;

            item.setData(novaData);

            tbvEntradas.getItems().add(item);
        }

        for (Saida item: saidaDAO.selecionarTodasSaidas()) {
            String novaData = "";
            String ano = "";
            String mes = "";
            String dia = "";

            int hifens = 0;

            for(int i = 0; i < item.getData().length(); i++) {
                if(i == 4 || i == 7) {
                    hifens++;
                    i++;
                }

                switch(hifens) {
                    case 0:
                        ano += item.getData().charAt(i);
                        break;
                    case 1:
                        mes += item.getData().charAt(i);
                        break;
                    case 2:
                        dia += item.getData().charAt(i);
                        break;
                }
            }

            novaData = dia + "/" + mes + "/" + ano;

            item.setData(novaData);
            tbvSaidas.getItems().add(item);
        }
    }

    public void voltar(ActionEvent actionEvent) {
        Stage stage = (Stage) tbvRotulos.getScene().getWindow();
        stage.close();
    }

    public void dadosEntrada(MouseEvent mouseEvent) throws IOException {
        Entrada entrada;

        entrada = tbvEntradas.getSelectionModel().getSelectedItem();
        Global.Id = entrada.getId();
        Global.Data = entrada.getData();

        carregarDados();
    }

    public void dadosSaida(MouseEvent mouseEvent) throws IOException {
        Saida saida;

        saida = tbvSaidas.getSelectionModel().getSelectedItem();
        Global.Id = saida.getId();
        Global.Tipo = 2;
        Global.Data = saida.getData();

        carregarDados();
    }

    private void carregarDados() throws IOException {
        Stage dadosEntradaSaida = new Stage();

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/zeus/view/dadosEntradaSaida.fxml"));
        Parent root = fxmlLoader.load();

        Scene scene = new Scene(root);
        dadosEntradaSaida.setScene(scene);

        dadosEntradaSaida.show();
    }
}
