package zeus.control;

import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import zeus.dao.EntradaDAO;
import zeus.dao.RotuloDAO;
import zeus.dao.SaidaDAO;
import zeus.model.Rotulo;
import zeus.model.RotuloEntradaSaida;

import java.util.ArrayList;

public class CadastroEntradaSaidaController {
    public ComboBox spnRotulo;
    public TextField txtQuantidade;
    public Button btnAdicionar;
    public Button btnRemover;
    public TableView<RotuloEntradaSaida> tbvEntradaSaida;
    public TableColumn<RotuloEntradaSaida, String> tvRotulo;
    public TableColumn<RotuloEntradaSaida, Integer> tvQuantidade;
    public RadioButton rdbEntrada;
    public RadioButton rdbSaida;

    private ArrayList<RotuloEntradaSaida> listaRotuloEntradaSaida = new ArrayList<>();

    public void initialize() {
        tvRotulo.setCellValueFactory(new PropertyValueFactory<>("NomeRotulo"));
        tvQuantidade.setCellValueFactory(new PropertyValueFactory<>("Quantidade"));
        RotuloDAO rotuloDAO = new RotuloDAO();

        ToggleGroup group = new ToggleGroup();

        rdbEntrada.setToggleGroup(group);
        rdbSaida.setToggleGroup(group);

        rdbEntrada.setSelected(true);

        ArrayList<Rotulo> lista = rotuloDAO.selecionarTodosRotulos();

        for (Rotulo rotulo: lista) {
            spnRotulo.getItems().add(rotulo.getNomeRotulo());
        }
    }

    public void adicionarRotulo() {
        RotuloDAO rotuloDAO = new RotuloDAO();

        RotuloEntradaSaida rotuloEntradaSaida = new RotuloEntradaSaida(0, rotuloDAO.selecionarIdRotulo(String.valueOf(spnRotulo.getValue())), Integer.parseInt(txtQuantidade.getText()), String.valueOf(spnRotulo.getValue()));

        listaRotuloEntradaSaida.add(rotuloEntradaSaida);

        tbvEntradaSaida.getItems().add(rotuloEntradaSaida);
    }

    public void removerRotulo() {
        RotuloEntradaSaida rotulo = tbvEntradaSaida.getSelectionModel().getSelectedItem();
        tbvEntradaSaida.getItems().remove(rotulo);

        listaRotuloEntradaSaida.remove(rotulo);
    }

    public void cadastrar() {
        boolean status;

        if(rdbEntrada.isSelected()) {
            EntradaDAO entradaDAO = new EntradaDAO();

            status = entradaDAO.cadastrarEntrada(listaRotuloEntradaSaida);
        } else {
            SaidaDAO saidaDAO = new SaidaDAO();

            status = saidaDAO.cadastrarSaida(listaRotuloEntradaSaida);
        }

        if(status) {
            tbvEntradaSaida.getItems().clear();
        }
    }

    public void cancelar() {
        Stage stage = (Stage) rdbEntrada.getScene().getWindow();
        stage.close();
    }
}
