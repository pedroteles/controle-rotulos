package zeus.control;

import javafx.event.ActionEvent;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import zeus.dao.RotuloDAO;
import zeus.model.Rotulo;

public class CadastroRotuloController {
    public TextField txtNomeRotulo;

    public void cadastrarRotulo(ActionEvent actionEvent) {
        Rotulo rotulo = new Rotulo();
        RotuloDAO rotuloDAO = new RotuloDAO();

        rotulo.setNomeRotulo(txtNomeRotulo.getText());

        rotuloDAO.cadastrarRotulo(rotulo);
    }

    public void cancelar(ActionEvent actionEvent) {
        Stage stage = (Stage) txtNomeRotulo.getScene().getWindow();
        stage.close();
    }
}
