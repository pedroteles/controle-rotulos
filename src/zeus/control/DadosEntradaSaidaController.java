package zeus.control;

import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import zeus.Global;
import zeus.dao.EntradaDAO;
import zeus.dao.SaidaDAO;
import zeus.model.RotuloEntradaSaida;

public class DadosEntradaSaidaController {
    public Label lblDia;
    public TableView<RotuloEntradaSaida> tbvEntradaSaida;
    public TableColumn<RotuloEntradaSaida, String> tcNomeRotulo;
    public TableColumn<RotuloEntradaSaida, Integer> tcQuantidade;

    public void initialize() {
        tcNomeRotulo.setCellValueFactory(new PropertyValueFactory<>("NomeRotulo"));
        tcQuantidade.setCellValueFactory(new PropertyValueFactory<>("Quantidade"));

        lblDia.setText(Global.Data);

        if(Global.Tipo == 1) {
            EntradaDAO entradaDAO = new EntradaDAO();

            for (RotuloEntradaSaida item: entradaDAO.selecionarDadosEntrada(Global.Id)) {
                tbvEntradaSaida.getItems().add(item);
            }

        } else {
            SaidaDAO saidaDAO = new SaidaDAO();

            for (RotuloEntradaSaida item: saidaDAO.selecionarDadosSaida(Global.Id)) {
                tbvEntradaSaida.getItems().add(item);
            }
        }
    }

    public void voltar() {
        Stage stage = (Stage) lblDia.getScene().getWindow();
        stage.close();
    }
}
