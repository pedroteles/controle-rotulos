package zeus.control;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import zeus.dao.RotuloDAO;
import zeus.model.Rotulo;

import java.io.IOException;
import java.util.ArrayList;

public class MenuController {
    public void cadastrarRotulo() throws IOException {
        Stage cadastroRotulo = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("/zeus/view/cadastroRotulo.fxml"));
        cadastroRotulo.setTitle("Cadastro de Rótulos");
        cadastroRotulo.setScene(new Scene(root));
        cadastroRotulo.show();
    }

    public void cadastrarEntrada() throws IOException {
        RotuloDAO rotuloDAO = new RotuloDAO();

        ArrayList<Rotulo> lista = rotuloDAO.selecionarTodosRotulos();

        if(!lista.isEmpty()) {
            Stage cadastroEntrada = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("/zeus/view/cadastroEntradaSaida.fxml"));
            cadastroEntrada.setTitle("Cadastro de Entrada/Saída");
            cadastroEntrada.setScene(new Scene(root));
            cadastroEntrada.show();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erro!");
            alert.setHeaderText("Não há rótulos cadastrados!");
            alert.setContentText("Cadastre um Rótulo para poder cadastrar\nEntradas e Saídas!");

            alert.showAndWait();
        }
    }

    public void bancoDados() throws IOException {
        Stage bancoDados = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("/zeus/view/bancoDados.fxml"));
        bancoDados.setTitle("Banco de Dados");
        bancoDados.setScene(new Scene(root));
        bancoDados.show();
    }
}
