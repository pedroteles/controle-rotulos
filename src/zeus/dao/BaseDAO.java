package zeus.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BaseDAO {
    public static String status = "Não conectou...";

    public BaseDAO() {

    }

    public static Connection getConexao() {
        Connection connection = null;

        try {
            String driverName = "org.sqlite.JDBC";

            Class.forName(driverName);

            String url = "jdbc:sqlite::resource:zeusDB.sqlite";

            try {
                connection = DriverManager.getConnection(url);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            if (connection != null) {
                status = ("STATUS--->Conectado com sucesso!");
            } else {
                status = ("STATUS--->Não foi possivel realizar conexão");
            }

            return connection;

        } catch (ClassNotFoundException e) {
            System.out.println("O driver expecificado nao foi encontrado.");

            return null;
        }
    }

    public static String statusConection() {
        return status;
    }

    public static boolean FecharConexao() {
        try {
            BaseDAO.getConexao().close();

            return true;

        } catch (SQLException e) {
            return false;
        }
    }

    public static Connection ReiniciarConexao() {
        FecharConexao();

        return BaseDAO.getConexao();
    }
}
