package zeus.dao;

import javafx.scene.control.Alert;
import zeus.model.Entrada;
import zeus.model.RotuloEntradaSaida;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

public class EntradaDAO extends BaseDAO {
    public boolean cadastrarEntrada(ArrayList<RotuloEntradaSaida> lista) {
        Connection conn = getConexao();

        String sql = "INSERT INTO entrada(data) VALUES (?)";

        try {
            PreparedStatement cadastrarEntrada = conn.prepareStatement(sql);

            Date date = new Date();
            LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

            int ano = localDate.getYear();
            int mes = localDate.getMonthValue();
            int dia = localDate.getDayOfMonth();

            String data = String.valueOf(ano) + "-" + String.valueOf(mes) + "-" + String.valueOf(dia);

            cadastrarEntrada.setString(1, data);

            cadastrarEntrada.execute();

            sql = "SELECT seq FROM sqlite_sequence WHERE NAME=\"entrada\"";

            PreparedStatement ultimoIdEntrada = conn.prepareStatement(sql);

            int idEntrada = 0;

            ResultSet rsUltimoId = ultimoIdEntrada.executeQuery();

            while (rsUltimoId.next()) {
                idEntrada = rsUltimoId.getInt("seq");
            }

            sql = "INSERT INTO rotulo_entrada (idRotulo, idEntrada, quantidade) VALUES (?, ?, ?)";

            for (RotuloEntradaSaida rotuloEntradaSaida :lista) {
                PreparedStatement inserirRotuloEntrada = conn.prepareStatement(sql);

                inserirRotuloEntrada.setInt(1, rotuloEntradaSaida.getIdRotulo());
                inserirRotuloEntrada.setInt(2, idEntrada);
                inserirRotuloEntrada.setInt(3, rotuloEntradaSaida.getQuantidade());

                inserirRotuloEntrada.execute();
            }

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Entrada Cadastrada!");
            alert.setHeaderText("Entrada cadastrada com sucesso!");

            alert.showAndWait();

            conn.close();

            return true;

        } catch (Exception e) {
            e.printStackTrace();

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erro!");
            alert.setHeaderText("Erro ao processar a operação!");
            alert.setContentText("Tente novamente.");

            alert.showAndWait();

            return false;
        }
    }

    public ArrayList<Entrada> selecionarTodasEntradas() {
        Connection conn = getConexao();

        ArrayList<Entrada> lista = new ArrayList<>();

        String sql = "SELECT id, data FROM entrada";

        try {
            PreparedStatement selecionarTodasEntradas = conn.prepareStatement(sql);

            ResultSet rs = selecionarTodasEntradas.executeQuery();

            while(rs.next()) {
                Entrada entrada = new Entrada();

                entrada.setId(rs.getInt("id"));
                entrada.setData(rs.getString("data"));

                lista.add(entrada);
            }

            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lista;
    }

    public ArrayList<RotuloEntradaSaida> selecionarDadosEntrada(int idEntrada) {
        Connection conn = getConexao();

        ArrayList<RotuloEntradaSaida> lista = new ArrayList<>();

        String sql = "SELECT rotulo.nomeRotulo, rotulo_entrada.quantidade FROM rotulo_entrada\n" +
                "INNER JOIN rotulo ON rotulo.id = rotulo_entrada.idRotulo WHERE rotulo_entrada.idEntrada = ?;";

        try {
            PreparedStatement selecionarDadosEntrada = conn.prepareStatement(sql);

            selecionarDadosEntrada.setInt(1, idEntrada);

            ResultSet rs = selecionarDadosEntrada.executeQuery();

            while(rs.next()) {
                RotuloEntradaSaida rotuloEntradaSaida = new RotuloEntradaSaida();

                rotuloEntradaSaida.setNomeRotulo(rs.getString("nomeRotulo"));
                rotuloEntradaSaida.setQuantidade(rs.getInt("quantidade"));

                lista.add(rotuloEntradaSaida);
            }

            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lista;
    }
}
