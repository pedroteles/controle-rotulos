package zeus.dao;

import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import zeus.model.Rotulo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class RotuloDAO extends BaseDAO {
    public void cadastrarRotulo(Rotulo rotulo) {
        Connection conn = getConexao();

        String sql = "INSERT INTO rotulo(nomeRotulo) VALUES(?)";

        try {
            PreparedStatement cadastrarRotulo = conn.prepareStatement(sql);

            cadastrarRotulo.setString(1, rotulo.getNomeRotulo());

            cadastrarRotulo.execute();

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Rótulo Cadastrado!");
            alert.setHeaderText("Rótulo cadastrado com sucesso!");

            alert.showAndWait();

            conn.close();
        } catch (Exception e) {
            e.printStackTrace();

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erro!");
            alert.setHeaderText("Erro ao processar a operação!");
            alert.setContentText("Tente novamente.");

            alert.showAndWait();
        }
    }

    public ArrayList<Rotulo> selecionarTodosRotulos() {
        Connection conn = getConexao();

        ArrayList<Rotulo> listaRotulos = new ArrayList<Rotulo>();

        String sql = "SELECT id, nomeRotulo FROM rotulo";

        try {
            PreparedStatement selecionarTodosRotulos = conn.prepareStatement(sql);

            ResultSet rs = selecionarTodosRotulos.executeQuery();

            while(rs.next()) {
                Rotulo rotulo = new Rotulo();

                rotulo.setId(rs.getInt("id"));
                rotulo.setNomeRotulo(rs.getString("nomeRotulo"));

                listaRotulos.add(rotulo);
            }

            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listaRotulos;
    }

    public int selecionarIdRotulo(String nomeRotulo) {
        Connection conn = getConexao();

        int id = 0;

        String sql = "SELECT id FROM rotulo WHERE nomeRotulo = ?";

        try {
            PreparedStatement selecionarIdRotulo = conn.prepareStatement(sql);

            selecionarIdRotulo.setString(1, nomeRotulo);

            ResultSet rs = selecionarIdRotulo.executeQuery();

            if(rs != null) {
                while(rs.next()) {
                    id = rs.getInt("id");
                }
            }

            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return id;
    }
}
