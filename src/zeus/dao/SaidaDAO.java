package zeus.dao;

import javafx.scene.control.Alert;
import zeus.model.RotuloEntradaSaida;
import zeus.model.Saida;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

public class SaidaDAO extends BaseDAO {
    public boolean cadastrarSaida(ArrayList<RotuloEntradaSaida> lista) {
        Connection conn = getConexao();

        String sql = "INSERT INTO saida(data) VALUES (?)";

        try {
            PreparedStatement cadastrarSaida = conn.prepareStatement(sql);

            Date date = new Date();
            LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

            int ano = localDate.getYear();
            int mes = localDate.getMonthValue();
            int dia = localDate.getDayOfMonth();

            String data = String.valueOf(ano) + "-" + String.valueOf(mes) + "-" + String.valueOf(dia);

            cadastrarSaida.setString(1, data);

            cadastrarSaida.execute();

            sql = "SELECT seq FROM sqlite_sequence WHERE NAME=\"saida\"";

            PreparedStatement ultimoIdSaida = conn.prepareStatement(sql);

            int idSaida = 0;

            ResultSet rsUltimoId = ultimoIdSaida.executeQuery();

            while (rsUltimoId.next()) {
                idSaida = rsUltimoId.getInt("seq");
            }

            sql = "INSERT INTO rotulo_saida (idRotulo, idSaida, quantidade) VALUES (?, ?, ?)";

            for (RotuloEntradaSaida rotuloEntradaSaida :lista) {
                PreparedStatement inserirRotuloSaida = conn.prepareStatement(sql);

                inserirRotuloSaida.setInt(1, rotuloEntradaSaida.getIdRotulo());
                inserirRotuloSaida.setInt(2, idSaida);
                inserirRotuloSaida.setInt(3, rotuloEntradaSaida.getQuantidade());

                inserirRotuloSaida.execute();
            }

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Saída Cadastrada!");
            alert.setHeaderText("Saída cadastrada com sucesso!");

            alert.showAndWait();

            conn.close();

            return true;

        } catch (Exception e) {
            e.printStackTrace();

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erro!");
            alert.setHeaderText("Erro ao processar a operação!");
            alert.setContentText("Tente novamente.");

            alert.showAndWait();

            return false;
        }
    }

    public ArrayList<Saida> selecionarTodasSaidas() {
        Connection conn = getConexao();

        ArrayList<Saida> lista = new ArrayList<>();

        String sql = "SELECT idSaida, data FROM saida";

        try {
            PreparedStatement selecionarTodasSaidas = conn.prepareStatement(sql);

            ResultSet rs = selecionarTodasSaidas.executeQuery();

            while(rs.next()) {
                Saida saida = new Saida();

                saida.setId(rs.getInt("idSaida"));
                saida.setData(rs.getString("data"));

                lista.add(saida);
            }

            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lista;
    }

    public ArrayList<RotuloEntradaSaida> selecionarDadosSaida(int idSaida) {
        Connection conn = getConexao();

        ArrayList<RotuloEntradaSaida> lista = new ArrayList<>();

        String sql = "SELECT rotulo.nomeRotulo, rotulo_saida.quantidade FROM rotulo_saida\n" +
                "INNER JOIN rotulo ON rotulo.id = rotulo_saida.idRotulo WHERE rotulo_saida.idSaida = ?;";

        try {
            PreparedStatement selecionarDadosSaida = conn.prepareStatement(sql);

            selecionarDadosSaida.setInt(1, idSaida);

            ResultSet rs = selecionarDadosSaida.executeQuery();

            while(rs.next()) {
                RotuloEntradaSaida rotuloEntradaSaida = new RotuloEntradaSaida();

                rotuloEntradaSaida.setNomeRotulo(rs.getString("nomeRotulo"));
                rotuloEntradaSaida.setQuantidade(rs.getInt("quantidade"));

                lista.add(rotuloEntradaSaida);
            }

            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lista;
    }
}
