package zeus.model;

public class Entrada {
    private int Id;
    private String Data;

    public Entrada() {
        this.Id = 0;
        this.Data = "";
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getData() {
        return Data;
    }

    public void setData(String data) {
        Data = data;
    }
}
