package zeus.model;

public class Rotulo {
    private int Id;
    private String NomeRotulo;

    public Rotulo() {
        this.Id = 0;
        this.NomeRotulo = "";
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getNomeRotulo() {
        return NomeRotulo;
    }

    public void setNomeRotulo(String nomeRotulo) {
        NomeRotulo = nomeRotulo;
    }
}
