package zeus.model;

public class RotuloEntradaSaida {
    private int Id;
    private int IdRotulo;
    private int Quantidade;
    private String NomeRotulo;

    public RotuloEntradaSaida() {
        this.Id = 0;
        this.IdRotulo = 0;
        this.Quantidade = 0;
    }

    public RotuloEntradaSaida(int id, int idRotulo, int quantidade, String nomeRotulo) {
        this.Id = id;
        this.IdRotulo = idRotulo;
        this.Quantidade = quantidade;
        this.NomeRotulo = nomeRotulo;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getIdRotulo() {
        return IdRotulo;
    }

    public void setIdRotulo(int idRotulo) {
        IdRotulo = idRotulo;
    }

    public int getQuantidade() {
        return Quantidade;
    }

    public void setQuantidade(int quantidade) {
        Quantidade = quantidade;
    }

    public String getNomeRotulo() {
        return NomeRotulo;
    }

    public void setNomeRotulo(String nomeRotulo) {
        NomeRotulo = nomeRotulo;
    }
}
