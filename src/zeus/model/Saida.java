package zeus.model;

public class Saida {
    private int Id;
    private String Data;

    public Saida() {
        this.Id = 0;
        this.Data = "";
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getData() {
        return Data;
    }

    public void setData(String data) {
        Data = data;
    }
}
